#ifndef __FLEX_GRAMMAR_H_
#define __FLEX_GRAMMAR_H_
#define __TOKEN_NUM 28
#define __NTERM_LEN 3
#define __TERM_LEN  25
#define __S file

#define is_terminal(token) ((uint8_t)((token & 0x40000000) >> 30))
#define token_value(token) ((uint32_t)(token & 0xBFFFFFFF))
#define gr_term_key(token) (token_value(token) - __NTERM_LEN)
#define gr_nterm_key(token)(token_value(token))
#define gr_term_token(key) ((gr_token)(0xBFFFFFFF | (key + __NTERM_LEN)))
#define gr_nterm_token(key) ((gr_token)key)

typedef enum gr_token {
  file = 0, document, elements,
  BINT32 = 0x40000003, END, INDEX, DOUBLE, UTF8_STRING, BINDATA, UNDEFINED, OBJECTID, FALSE, TRUE, UTC, NIL, REGEX, DBPOINTER, JSCODE, DEPRECATED, INT32, TIMESTAMP, INT64, MINKEY, MAXKEY, BYTE, EMBDOC, ARRAY, __TERM
} gr_token;

#endif
