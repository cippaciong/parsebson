#ifndef __GRAMMAR_SEMANTICS_H_
#define __GRAMMAR_SEMANTICS_H_

#include "token_node.h"
#include "vect_stack.h"
#include "parsing_context.h"

void semantic_fun(uint32_t rule_number, token_node *p, vect_stack *stack, parsing_ctx *ctx);


#endif
