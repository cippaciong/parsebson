require 'json'
require 'bson'

# Usage: 'ruby bsonizer.rb filename' (without the .json extension)
# The bson-ruby gem is required https://github.com/mongodb/bson-ruby

def encode(filename)
  jsonfile = File.read("#{filename}.json")
  json = JSON.parse(jsonfile)

  File.open("#{filename}.bson","w") do |f|
		f.write(json.to_bson)
  end
end

encode(ARGV[0])
