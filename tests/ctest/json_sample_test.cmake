# SERVLET
execute_process(COMMAND ${SOURCEDIR}/bin/bson_parser "${SOURCEDIR}/tests/samples/json_sample.bson"
                OUTPUT_FILE ${SOURCEDIR}/tests/output/json_sample.txt
                )
execute_process(COMMAND sed "-i" "/Lexer/d" "${SOURCEDIR}/tests/output/json_sample.txt"
                OUTPUT_QUIET
                )

execute_process(COMMAND sed "-i" "/Parser/d" "${SOURCEDIR}/tests/output/json_sample.txt"
                OUTPUT_QUIET
                )


execute_process(COMMAND ${CMAKE_COMMAND} -E compare_files
    ${SOURCEDIR}/tests/output/json_sample.txt ${SOURCEDIR}/tests/expected_output/json_sample.txt
    RESULT_VARIABLE DIFFERENT)
if(DIFFERENT)
    message(FATAL_ERROR "Test failed - files differ")
endif()

