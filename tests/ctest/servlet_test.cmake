# SERVLET
execute_process(COMMAND ${SOURCEDIR}/bin/bson_parser "${SOURCEDIR}/tests/samples/servlet.bson"
                OUTPUT_FILE ${SOURCEDIR}/tests/output/servlet.txt
                )
execute_process(COMMAND sed "-i" "/Lexer/d" "${SOURCEDIR}/tests/output/servlet.txt"
                OUTPUT_QUIET
                )

execute_process(COMMAND sed "-i" "/Parser/d" "${SOURCEDIR}/tests/output/servlet.txt"
                OUTPUT_QUIET
                )


execute_process(COMMAND ${CMAKE_COMMAND} -E compare_files
    ${SOURCEDIR}/tests/output/servlet.txt ${SOURCEDIR}/tests/expected_output/servlet.txt
    RESULT_VARIABLE DIFFERENT)
if(DIFFERENT)
    message(FATAL_ERROR "Test failed - files differ")
endif()

