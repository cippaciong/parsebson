#include "grammar_semantics.h"

/* Preamble from grammar definition. */
/* End of the preamble. */

void semantic_fun(uint32_t rule_number, token_node *p, vect_stack *stack, parsing_ctx *ctx){

  switch(rule_number){
    case 0:
    {
      // variables declaration
      token_node *p_file, *p_document1;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = file;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_file = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_document1 = ctx->token_list;
        ctx->token_list = p_file;
      } else {
        p_document1 = p->next;
      }
      p->next = p_file;
      p_document1->parent = p_file;
      p_file->next = p_document1->next;
      p_file->child = p_document1;
      /* Semantic action follows. */
      {  }
      /* End of semantic action. */
    }
    break;
    case 1:
    {
      // variables declaration
      token_node *p_file, *p_document1, *p_BINT322, *p_END3;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = file;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_file = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_document1 = ctx->token_list;
        ctx->token_list = p_file;
      } else {
        p_document1 = p->next;
      }
      p->next = p_file;
      p_BINT322 = p_document1->next;
      p_END3 = p_BINT322->next;
      p_document1->parent = p_file;
      p_BINT322->parent = p_file;
      p_END3->parent = p_file;
      p_file->next = p_END3->next;
      p_file->child = p_document1;
      /* Semantic action follows. */
      {  }
      /* End of semantic action. */
    }
    break;
    case 2:
    {
      // variables declaration
      token_node *p_file, *p_document1, *p_BINT322, *p_elements3, *p_END4;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = file;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_file = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_document1 = ctx->token_list;
        ctx->token_list = p_file;
      } else {
        p_document1 = p->next;
      }
      p->next = p_file;
      p_BINT322 = p_document1->next;
      p_elements3 = p_BINT322->next;
      p_END4 = p_elements3->next;
      p_document1->parent = p_file;
      p_BINT322->parent = p_file;
      p_elements3->parent = p_file;
      p_END4->parent = p_file;
      p_file->next = p_END4->next;
      p_file->child = p_document1;
      /* Semantic action follows. */
      {  }
      /* End of semantic action. */
    }
    break;
    case 3:
    {
      // variables declaration
      token_node *p_document, *p_BINT321, *p_END2;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = document;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_document = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_BINT321 = ctx->token_list;
        ctx->token_list = p_document;
      } else {
        p_BINT321 = p->next;
      }
      p->next = p_document;
      p_END2 = p_BINT321->next;
      p_BINT321->parent = p_document;
      p_END2->parent = p_document;
      p_document->next = p_END2->next;
      p_document->child = p_BINT321;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 4:
    {
      // variables declaration
      token_node *p_document, *p_BINT321, *p_elements2, *p_END3;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = document;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_document = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_BINT321 = ctx->token_list;
        ctx->token_list = p_document;
      } else {
        p_BINT321 = p->next;
      }
      p->next = p_document;
      p_elements2 = p_BINT321->next;
      p_END3 = p_elements2->next;
      p_BINT321->parent = p_document;
      p_elements2->parent = p_document;
      p_END3->parent = p_document;
      p_document->next = p_END3->next;
      p_document->child = p_BINT321;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 5:
    {
      // variables declaration
      token_node *p_elements, *p_elements1, *p_INDEX2;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_elements1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_elements1 = p->next;
      }
      p->next = p_elements;
      p_INDEX2 = p_elements1->next;
      p_elements1->parent = p_elements;
      p_INDEX2->parent = p_elements;
      p_elements->next = p_INDEX2->next;
      p_elements->child = p_elements1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 6:
    {
      // variables declaration
      token_node *p_elements, *p_elements1, *p_DOUBLE2;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_elements1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_elements1 = p->next;
      }
      p->next = p_elements;
      p_DOUBLE2 = p_elements1->next;
      p_elements1->parent = p_elements;
      p_DOUBLE2->parent = p_elements;
      p_elements->next = p_DOUBLE2->next;
      p_elements->child = p_elements1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 7:
    {
      // variables declaration
      token_node *p_elements, *p_elements1, *p_UTF8_STRING2;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_elements1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_elements1 = p->next;
      }
      p->next = p_elements;
      p_UTF8_STRING2 = p_elements1->next;
      p_elements1->parent = p_elements;
      p_UTF8_STRING2->parent = p_elements;
      p_elements->next = p_UTF8_STRING2->next;
      p_elements->child = p_elements1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 8:
    {
      // variables declaration
      token_node *p_elements, *p_elements1, *p_BINDATA2;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_elements1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_elements1 = p->next;
      }
      p->next = p_elements;
      p_BINDATA2 = p_elements1->next;
      p_elements1->parent = p_elements;
      p_BINDATA2->parent = p_elements;
      p_elements->next = p_BINDATA2->next;
      p_elements->child = p_elements1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 9:
    {
      // variables declaration
      token_node *p_elements, *p_elements1, *p_UNDEFINED2;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_elements1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_elements1 = p->next;
      }
      p->next = p_elements;
      p_UNDEFINED2 = p_elements1->next;
      p_elements1->parent = p_elements;
      p_UNDEFINED2->parent = p_elements;
      p_elements->next = p_UNDEFINED2->next;
      p_elements->child = p_elements1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 10:
    {
      // variables declaration
      token_node *p_elements, *p_elements1, *p_OBJECTID2;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_elements1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_elements1 = p->next;
      }
      p->next = p_elements;
      p_OBJECTID2 = p_elements1->next;
      p_elements1->parent = p_elements;
      p_OBJECTID2->parent = p_elements;
      p_elements->next = p_OBJECTID2->next;
      p_elements->child = p_elements1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 11:
    {
      // variables declaration
      token_node *p_elements, *p_elements1, *p_FALSE2;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_elements1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_elements1 = p->next;
      }
      p->next = p_elements;
      p_FALSE2 = p_elements1->next;
      p_elements1->parent = p_elements;
      p_FALSE2->parent = p_elements;
      p_elements->next = p_FALSE2->next;
      p_elements->child = p_elements1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 12:
    {
      // variables declaration
      token_node *p_elements, *p_elements1, *p_TRUE2;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_elements1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_elements1 = p->next;
      }
      p->next = p_elements;
      p_TRUE2 = p_elements1->next;
      p_elements1->parent = p_elements;
      p_TRUE2->parent = p_elements;
      p_elements->next = p_TRUE2->next;
      p_elements->child = p_elements1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 13:
    {
      // variables declaration
      token_node *p_elements, *p_elements1, *p_UTC2;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_elements1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_elements1 = p->next;
      }
      p->next = p_elements;
      p_UTC2 = p_elements1->next;
      p_elements1->parent = p_elements;
      p_UTC2->parent = p_elements;
      p_elements->next = p_UTC2->next;
      p_elements->child = p_elements1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 14:
    {
      // variables declaration
      token_node *p_elements, *p_elements1, *p_NIL2;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_elements1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_elements1 = p->next;
      }
      p->next = p_elements;
      p_NIL2 = p_elements1->next;
      p_elements1->parent = p_elements;
      p_NIL2->parent = p_elements;
      p_elements->next = p_NIL2->next;
      p_elements->child = p_elements1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 15:
    {
      // variables declaration
      token_node *p_elements, *p_elements1, *p_REGEX2;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_elements1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_elements1 = p->next;
      }
      p->next = p_elements;
      p_REGEX2 = p_elements1->next;
      p_elements1->parent = p_elements;
      p_REGEX2->parent = p_elements;
      p_elements->next = p_REGEX2->next;
      p_elements->child = p_elements1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 16:
    {
      // variables declaration
      token_node *p_elements, *p_elements1, *p_DBPOINTER2;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_elements1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_elements1 = p->next;
      }
      p->next = p_elements;
      p_DBPOINTER2 = p_elements1->next;
      p_elements1->parent = p_elements;
      p_DBPOINTER2->parent = p_elements;
      p_elements->next = p_DBPOINTER2->next;
      p_elements->child = p_elements1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 17:
    {
      // variables declaration
      token_node *p_elements, *p_elements1, *p_JSCODE2;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_elements1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_elements1 = p->next;
      }
      p->next = p_elements;
      p_JSCODE2 = p_elements1->next;
      p_elements1->parent = p_elements;
      p_JSCODE2->parent = p_elements;
      p_elements->next = p_JSCODE2->next;
      p_elements->child = p_elements1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 18:
    {
      // variables declaration
      token_node *p_elements, *p_elements1, *p_DEPRECATED2;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_elements1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_elements1 = p->next;
      }
      p->next = p_elements;
      p_DEPRECATED2 = p_elements1->next;
      p_elements1->parent = p_elements;
      p_DEPRECATED2->parent = p_elements;
      p_elements->next = p_DEPRECATED2->next;
      p_elements->child = p_elements1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 19:
    {
      // variables declaration
      token_node *p_elements, *p_elements1, *p_INT322;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_elements1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_elements1 = p->next;
      }
      p->next = p_elements;
      p_INT322 = p_elements1->next;
      p_elements1->parent = p_elements;
      p_INT322->parent = p_elements;
      p_elements->next = p_INT322->next;
      p_elements->child = p_elements1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 20:
    {
      // variables declaration
      token_node *p_elements, *p_elements1, *p_TIMESTAMP2;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_elements1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_elements1 = p->next;
      }
      p->next = p_elements;
      p_TIMESTAMP2 = p_elements1->next;
      p_elements1->parent = p_elements;
      p_TIMESTAMP2->parent = p_elements;
      p_elements->next = p_TIMESTAMP2->next;
      p_elements->child = p_elements1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 21:
    {
      // variables declaration
      token_node *p_elements, *p_elements1, *p_INT642;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_elements1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_elements1 = p->next;
      }
      p->next = p_elements;
      p_INT642 = p_elements1->next;
      p_elements1->parent = p_elements;
      p_INT642->parent = p_elements;
      p_elements->next = p_INT642->next;
      p_elements->child = p_elements1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 22:
    {
      // variables declaration
      token_node *p_elements, *p_elements1, *p_MINKEY2;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_elements1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_elements1 = p->next;
      }
      p->next = p_elements;
      p_MINKEY2 = p_elements1->next;
      p_elements1->parent = p_elements;
      p_MINKEY2->parent = p_elements;
      p_elements->next = p_MINKEY2->next;
      p_elements->child = p_elements1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 23:
    {
      // variables declaration
      token_node *p_elements, *p_elements1, *p_MAXKEY2;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_elements1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_elements1 = p->next;
      }
      p->next = p_elements;
      p_MAXKEY2 = p_elements1->next;
      p_elements1->parent = p_elements;
      p_MAXKEY2->parent = p_elements;
      p_elements->next = p_MAXKEY2->next;
      p_elements->child = p_elements1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 24:
    {
      // variables declaration
      token_node *p_elements, *p_elements1, *p_BYTE2;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_elements1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_elements1 = p->next;
      }
      p->next = p_elements;
      p_BYTE2 = p_elements1->next;
      p_elements1->parent = p_elements;
      p_BYTE2->parent = p_elements;
      p_elements->next = p_BYTE2->next;
      p_elements->child = p_elements1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 25:
    {
      // variables declaration
      token_node *p_elements, *p_elements1, *p_EMBDOC2, *p_END3;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_elements1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_elements1 = p->next;
      }
      p->next = p_elements;
      p_EMBDOC2 = p_elements1->next;
      p_END3 = p_EMBDOC2->next;
      p_elements1->parent = p_elements;
      p_EMBDOC2->parent = p_elements;
      p_END3->parent = p_elements;
      p_elements->next = p_END3->next;
      p_elements->child = p_elements1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 26:
    {
      // variables declaration
      token_node *p_elements, *p_elements1, *p_EMBDOC2, *p_elements3, *p_END4;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_elements1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_elements1 = p->next;
      }
      p->next = p_elements;
      p_EMBDOC2 = p_elements1->next;
      p_elements3 = p_EMBDOC2->next;
      p_END4 = p_elements3->next;
      p_elements1->parent = p_elements;
      p_EMBDOC2->parent = p_elements;
      p_elements3->parent = p_elements;
      p_END4->parent = p_elements;
      p_elements->next = p_END4->next;
      p_elements->child = p_elements1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 27:
    {
      // variables declaration
      token_node *p_elements, *p_elements1, *p_ARRAY2, *p_END3;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_elements1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_elements1 = p->next;
      }
      p->next = p_elements;
      p_ARRAY2 = p_elements1->next;
      p_END3 = p_ARRAY2->next;
      p_elements1->parent = p_elements;
      p_ARRAY2->parent = p_elements;
      p_END3->parent = p_elements;
      p_elements->next = p_END3->next;
      p_elements->child = p_elements1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 28:
    {
      // variables declaration
      token_node *p_elements, *p_elements1, *p_ARRAY2, *p_elements3, *p_END4;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_elements1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_elements1 = p->next;
      }
      p->next = p_elements;
      p_ARRAY2 = p_elements1->next;
      p_elements3 = p_ARRAY2->next;
      p_END4 = p_elements3->next;
      p_elements1->parent = p_elements;
      p_ARRAY2->parent = p_elements;
      p_elements3->parent = p_elements;
      p_END4->parent = p_elements;
      p_elements->next = p_END4->next;
      p_elements->child = p_elements1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 29:
    {
      // variables declaration
      token_node *p_elements, *p_INDEX1;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_INDEX1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_INDEX1 = p->next;
      }
      p->next = p_elements;
      p_INDEX1->parent = p_elements;
      p_elements->next = p_INDEX1->next;
      p_elements->child = p_INDEX1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 30:
    {
      // variables declaration
      token_node *p_elements, *p_DOUBLE1;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_DOUBLE1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_DOUBLE1 = p->next;
      }
      p->next = p_elements;
      p_DOUBLE1->parent = p_elements;
      p_elements->next = p_DOUBLE1->next;
      p_elements->child = p_DOUBLE1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 31:
    {
      // variables declaration
      token_node *p_elements, *p_UTF8_STRING1;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_UTF8_STRING1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_UTF8_STRING1 = p->next;
      }
      p->next = p_elements;
      p_UTF8_STRING1->parent = p_elements;
      p_elements->next = p_UTF8_STRING1->next;
      p_elements->child = p_UTF8_STRING1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 32:
    {
      // variables declaration
      token_node *p_elements, *p_BINDATA1;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_BINDATA1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_BINDATA1 = p->next;
      }
      p->next = p_elements;
      p_BINDATA1->parent = p_elements;
      p_elements->next = p_BINDATA1->next;
      p_elements->child = p_BINDATA1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 33:
    {
      // variables declaration
      token_node *p_elements, *p_UNDEFINED1;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_UNDEFINED1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_UNDEFINED1 = p->next;
      }
      p->next = p_elements;
      p_UNDEFINED1->parent = p_elements;
      p_elements->next = p_UNDEFINED1->next;
      p_elements->child = p_UNDEFINED1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 34:
    {
      // variables declaration
      token_node *p_elements, *p_OBJECTID1;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_OBJECTID1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_OBJECTID1 = p->next;
      }
      p->next = p_elements;
      p_OBJECTID1->parent = p_elements;
      p_elements->next = p_OBJECTID1->next;
      p_elements->child = p_OBJECTID1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 35:
    {
      // variables declaration
      token_node *p_elements, *p_FALSE1;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_FALSE1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_FALSE1 = p->next;
      }
      p->next = p_elements;
      p_FALSE1->parent = p_elements;
      p_elements->next = p_FALSE1->next;
      p_elements->child = p_FALSE1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 36:
    {
      // variables declaration
      token_node *p_elements, *p_TRUE1;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_TRUE1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_TRUE1 = p->next;
      }
      p->next = p_elements;
      p_TRUE1->parent = p_elements;
      p_elements->next = p_TRUE1->next;
      p_elements->child = p_TRUE1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 37:
    {
      // variables declaration
      token_node *p_elements, *p_UTC1;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_UTC1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_UTC1 = p->next;
      }
      p->next = p_elements;
      p_UTC1->parent = p_elements;
      p_elements->next = p_UTC1->next;
      p_elements->child = p_UTC1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 38:
    {
      // variables declaration
      token_node *p_elements, *p_NIL1;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_NIL1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_NIL1 = p->next;
      }
      p->next = p_elements;
      p_NIL1->parent = p_elements;
      p_elements->next = p_NIL1->next;
      p_elements->child = p_NIL1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 39:
    {
      // variables declaration
      token_node *p_elements, *p_REGEX1;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_REGEX1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_REGEX1 = p->next;
      }
      p->next = p_elements;
      p_REGEX1->parent = p_elements;
      p_elements->next = p_REGEX1->next;
      p_elements->child = p_REGEX1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 40:
    {
      // variables declaration
      token_node *p_elements, *p_DBPOINTER1;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_DBPOINTER1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_DBPOINTER1 = p->next;
      }
      p->next = p_elements;
      p_DBPOINTER1->parent = p_elements;
      p_elements->next = p_DBPOINTER1->next;
      p_elements->child = p_DBPOINTER1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 41:
    {
      // variables declaration
      token_node *p_elements, *p_JSCODE1;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_JSCODE1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_JSCODE1 = p->next;
      }
      p->next = p_elements;
      p_JSCODE1->parent = p_elements;
      p_elements->next = p_JSCODE1->next;
      p_elements->child = p_JSCODE1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 42:
    {
      // variables declaration
      token_node *p_elements, *p_DEPRECATED1;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_DEPRECATED1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_DEPRECATED1 = p->next;
      }
      p->next = p_elements;
      p_DEPRECATED1->parent = p_elements;
      p_elements->next = p_DEPRECATED1->next;
      p_elements->child = p_DEPRECATED1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 43:
    {
      // variables declaration
      token_node *p_elements, *p_INT321;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_INT321 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_INT321 = p->next;
      }
      p->next = p_elements;
      p_INT321->parent = p_elements;
      p_elements->next = p_INT321->next;
      p_elements->child = p_INT321;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 44:
    {
      // variables declaration
      token_node *p_elements, *p_TIMESTAMP1;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_TIMESTAMP1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_TIMESTAMP1 = p->next;
      }
      p->next = p_elements;
      p_TIMESTAMP1->parent = p_elements;
      p_elements->next = p_TIMESTAMP1->next;
      p_elements->child = p_TIMESTAMP1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 45:
    {
      // variables declaration
      token_node *p_elements, *p_INT641;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_INT641 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_INT641 = p->next;
      }
      p->next = p_elements;
      p_INT641->parent = p_elements;
      p_elements->next = p_INT641->next;
      p_elements->child = p_INT641;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 46:
    {
      // variables declaration
      token_node *p_elements, *p_MINKEY1;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_MINKEY1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_MINKEY1 = p->next;
      }
      p->next = p_elements;
      p_MINKEY1->parent = p_elements;
      p_elements->next = p_MINKEY1->next;
      p_elements->child = p_MINKEY1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 47:
    {
      // variables declaration
      token_node *p_elements, *p_MAXKEY1;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_MAXKEY1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_MAXKEY1 = p->next;
      }
      p->next = p_elements;
      p_MAXKEY1->parent = p_elements;
      p_elements->next = p_MAXKEY1->next;
      p_elements->child = p_MAXKEY1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 48:
    {
      // variables declaration
      token_node *p_elements, *p_BYTE1;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_BYTE1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_BYTE1 = p->next;
      }
      p->next = p_elements;
      p_BYTE1->parent = p_elements;
      p_elements->next = p_BYTE1->next;
      p_elements->child = p_BYTE1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 49:
    {
      // variables declaration
      token_node *p_elements, *p_EMBDOC1, *p_END2;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_EMBDOC1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_EMBDOC1 = p->next;
      }
      p->next = p_elements;
      p_END2 = p_EMBDOC1->next;
      p_EMBDOC1->parent = p_elements;
      p_END2->parent = p_elements;
      p_elements->next = p_END2->next;
      p_elements->child = p_EMBDOC1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 50:
    {
      // variables declaration
      token_node *p_elements, *p_EMBDOC1, *p_elements2, *p_END3;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_EMBDOC1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_EMBDOC1 = p->next;
      }
      p->next = p_elements;
      p_elements2 = p_EMBDOC1->next;
      p_END3 = p_elements2->next;
      p_EMBDOC1->parent = p_elements;
      p_elements2->parent = p_elements;
      p_END3->parent = p_elements;
      p_elements->next = p_END3->next;
      p_elements->child = p_EMBDOC1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 51:
    {
      // variables declaration
      token_node *p_elements, *p_ARRAY1, *p_END2;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_ARRAY1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_ARRAY1 = p->next;
      }
      p->next = p_elements;
      p_END2 = p_ARRAY1->next;
      p_ARRAY1->parent = p_elements;
      p_END2->parent = p_elements;
      p_elements->next = p_END2->next;
      p_elements->child = p_ARRAY1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
    case 52:
    {
      // variables declaration
      token_node *p_elements, *p_ARRAY1, *p_elements2, *p_END3;
      token_node *ttp = (token_node*) malloc(sizeof(token_node));
      ttp->token = elements;
      ttp->value = NULL;
      ttp->next = NULL;
      ttp->parent = NULL;
      ttp->child = NULL;
      p_elements = vect_stack_push(stack, ttp, ctx->NODE_REALLOC_SIZE);
      if (p->token == __TERM) {
        p_ARRAY1 = ctx->token_list;
        ctx->token_list = p_elements;
      } else {
        p_ARRAY1 = p->next;
      }
      p->next = p_elements;
      p_elements2 = p_ARRAY1->next;
      p_END3 = p_elements2->next;
      p_ARRAY1->parent = p_elements;
      p_elements2->parent = p_elements;
      p_END3->parent = p_elements;
      p_elements->next = p_END3->next;
      p_elements->child = p_ARRAY1;
      /* Semantic action follows. */
      { }
      /* End of semantic action. */
    }
    break;
  }
}